import CryptoJS from '@/packageA/utils/crypto-js/crypto-js.js';
import jj from '../md5.js';
const en = (str, key) => {
    if (!key) {
        return false;
    }
    let e, i;
    try {
        let iv = randomString(16),
            ia = jk([8, 21]),
            ib = jk([12, 14, 3, 4]),
            ic = jk([15, 0, 3, 3, 8, 16]);
        key = CryptoJS.enc.Utf8.parse(key);
        let encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(str), key, {
            [ia]: CryptoJS.enc.Utf8.parse(iv),
            [ib]: CryptoJS.mode.CBC,
            [ic]: CryptoJS.pad.Pkcs7
        });
        e = CryptoJS.enc.Hex.stringify(encrypted.ciphertext).toUpperCase(),
            i = CryptoJS.enc.Hex.stringify(CryptoJS.enc.Utf8.parse(iv)).toUpperCase();
    } catch (e) {
        console.log('oh~');
    }
    return e + jj(e.substr(2, 5)).toUpperCase() + i;
}
const de = (str, ur, k) => {
    if (!k || !str || !k) {
        return false;
    }
    let _str = '',
        ai = jk([8, 21]),
        ba = jk([12, 14, 3, 4]),
        bf = jk([15, 0, 3, 3, 8, 16]);
    try {
        let kk = jj(ur + k + '75636198730df897ce38c1d176c70f97'),
            _i, i, decrypted, cc = kk.substring(3, 19);
        str = CryptoJS.enc.Hex.parse(str).toString(CryptoJS.enc.Utf8);
        str = str.split(cc);
        k = CryptoJS.enc.Utf8.parse(kk);
        _i = CryptoJS.enc.Hex.parse(str[1]).toString(CryptoJS.enc.Utf8);
        i = CryptoJS.enc.Base64.parse(_i);
        decrypted = CryptoJS.AES.decrypt(str[0], k, {
            [ai]: i,
            [ba]: CryptoJS.mode.CBC,
            [bf]: CryptoJS.pad.Pkcs7
        });
        _str = decrypted.toString(CryptoJS.enc.Utf8).toString();
    } catch (e) {
        console.log('oh~');
    }
    return _str ? JSON.parse(_str) : false;
}
const randomString = (len) => {
    len = len || 32;
    var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
    var maxPos = $chars.length;
    var pwd = '';
    for (var i = 0; i < len; i++) {
        pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
}

const jk = (i) => {
    let s = 'abcdefghijklmnopqrstuvwxyz',
        z = '';
    i.forEach(v => {
        z += s.substr(v, 1);
    })
    return z;
}

module.exports = {
    en,
    de,
    randomString
}
