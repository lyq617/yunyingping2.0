const MyApp = getApp();
const globalData = MyApp.globalData;
export default {
    computed: {
        i18n() {
            return globalData.$t;
        }
    },
    data() {
        return {
            tarbarObj: globalData.tarbarObj,
            searchFocus: false,
            imageUrl: this.$config.imageUrl,
            apiImageUrl: this.$config.apiImageUrl,
            searchKey: '',
            searchList: [],
            scrollViewHeight: 0,
            swpierViewHeight: 0,
            historySearchList: [],
            loading: true,
            page: 1,
            tabLists: [
                {
                    name: globalData.$t('search').searchHistoryTitleBottom,
                    list: []
                },
                {
                    name: globalData.$t('search').hotSearch,
                    list: []
                },
                {
                    name: globalData.$t('search').movies,
                    list: []
                },
                {
                    name: globalData.$t('search').tvs,
                    list: []
                }
            ],
            typelist: globalData.typelist,
            // 因为内部的滑动机制限制，请将tabs组件和swiper组件的current用不同变量赋值
            current: 0, // tabs组件的current值，表示当前活动的tab选项
            swiperCurrent: 0, // swiper组件的current值，表示当前那个swiper-item是活动的
            isEnd: false
        };
    },
    onLoad() {
        this.searchFocus = true;
    },
    mounted() {
        this.$nextTick(function() {
            this.initScrollBox();
        });
    },
    onShow() {
        let _this = this;
        _this.tabIndex = uni.getStorageSync('tabIndex') || 0;
        uni.getStorage({
            key: 'historySearchList',
            complete(res) {
                if (res.data) {
                    _this.historySearchList = res.data;
                }
                _this.loading = false;
            }
        });
        uni.getStorage({
            key: 'historySearchListObj',
            complete(res) {
                if (res.data) {
                    _this.tabLists[0].list = res.data;
                }
            }
        });
    },
    methods: {
        clearHistorySearchList() {
            let _this = this;
            uni.showModal({
                title: _this.i18n('common').modelTitle,
                content: _this.i18n('search').confirmClearText,
                success(res) {
                    if (res.confirm) {
                        _this.historySearchList = [];
                        uni.removeStorageSync('historySearchList');
                    }
                }
            });
        },
        getSearchTabsList() {
            let _this = this;
            MyApp.showLoading(_this.$refs.loading);
            _this.$http
                .post('/videos/videos/searchTabs', {
                    type: _this.swiperCurrent
                })
                .then(res => {
                    MyApp.closeLoading(_this.$refs.loading);
                    if (res.code == 200) {
                        _this.tabLists[_this.swiperCurrent].list = res.data;
                        if (_this.swiperCurrent + 1 == _this.tabLists.length) {
                            uni.setStorageSync('searchTabsList', _this.tabLists);
                        }
                    } else {
                        uni.showToast({
                            title: res.msg || _this.i18n('common').networkError,
                            icon: 'none'
                        });
                    }
                }).catch(res => {
                    MyApp.closeLoading(_this.$refs.loading);
                });
        },
        doClear() {
            this.searchList = [];
            this.page = 1;
            this.isEnd = false;
        },
        navOnSearch() {
            this.searchList = [];
            this.page = 1;
            this.isEnd = false;
            this.onSearch();
        },
        onSearch(searchKey) {
            var _this = this;
            if (searchKey) {
                _this.searchKey = searchKey;
            }
            MyApp.showLoading(_this.$refs.loading);
            let historySearchList = uni.getStorageSync('historySearchList'),
                tmp = [];
            if (historySearchList.length == 0) {
                historySearchList = [];
            } else {
                historySearchList.forEach((v, k) => {
                    if (v.vod_name == _this.searchKey) {
                        historySearchList[k].vod_name = _this.searchKey;
                    } else {
                        tmp.push(v.vod_name);
                    }
                });
            }
            if (tmp.length == historySearchList.length) {
                historySearchList.unshift({
                    vod_name: _this.searchKey
                });
            }
            _this.historySearchList = historySearchList;
            uni.setStorageSync('historySearchList', historySearchList);
            
            if (searchKey) {
               searchKey = searchKey.replace(/(^\s*)|(\s*$)/g,"");
            }
            if (_this.searchKey) {
               _this.searchKey = _this.searchKey.replace(/(^\s*)|(\s*$)/g,"");
            }

            _this.$http
                .post('/videos/videos/searchKeywords', {
                    keyword: searchKey || _this.searchKey,
                    page: _this.page || 1
                })
                .then(res => {
                    MyApp.closeLoading(_this.$refs.loading);
                    if (res.code == 200) {
                        if (res.data.list.length > 0) {
                            res.data.list.forEach((v, k) => {
                                _this.searchList.push(v);
                            });
                            _this.tabLists[0].list = res.data.list;
                            uni.setStorageSync('historySearchListObj', res.data.list);
                        } else {
                            uni.showToast({
                                title: _this.i18n('search').noMore,
                                icon: 'none'
                            });
                            _this.isEnd = true;
                        }
                    } else {
                        uni.showToast({
                            title: res.msg || _this.i18n('common').networkError,
                            icon: 'none'
                        });
                    }
                    if (_this.searchList.length <= 0) {
                        uni.showToast({
                            title: _this.i18n('common').noSearchInfo,
                            icon: 'none'
                        });
                    }
                }).catch(res => {
                    MyApp.closeLoading(_this.$refs.loading);
                });
        },
        initScrollBox() {
            let windowHeight = uni.getSystemInfoSync().windowHeight;
            let query = uni.createSelectorQuery().in(this);
            query.select('#navbar').boundingClientRect();
            query.select('#header').boundingClientRect();
            query.select('#uTabbar').boundingClientRect();
            let _this = this;
            query.exec(res => {
                let navbarHeight = res[0].height;
                let headerHeight = res[1].height;
                let uTabbarHeight = res[2].height;
                _this.swpierViewHeight = windowHeight - navbarHeight - headerHeight - uTabbarHeight - 50;
                _this.scrollViewHeight = windowHeight - navbarHeight - uTabbarHeight - 50;
            });
        },
        goDetail(id) {
            MyApp.godetail(id);
        },
        // tabs通知swiper切换
        tabsChange(index) {
            this.swiperCurrent = index;
        },
        // swiper-item左右移动，通知tabs的滑块跟随移动
        transition(e) {
            let dx = e.detail.dx;
            this.$refs.uTabs.setDx(dx);
        },
        // 由于swiper的内部机制问题，快速切换swiper不会触发dx的连续变化，需要在结束时重置状态
        // swiper滑动结束，分别设置tabs和swiper的状态
        animationfinish(e) {
            let current = e.detail.current;
            this.$refs.uTabs.setFinishCurrent(current);
            this.current = current;
            this.swiperCurrent = current;
            if (current > 0) {
                let tmp = uni.getStorageSync('searchTabsList');
                tmp ? (this.tabLists = tmp) : this.getSearchTabsList();
            }
        },
        // scroll-view到底部加载更多
        onreachBottom() {
            this.page = this.page + 1;
            if (!this.isEnd) {
                this.onSearch();
            }
        }
    }
};
