### 使用

common/config.js 根据提示配置相关接口信息

根目录下有3个pages.json（H5 打包使用）、pages.json.app（APP端打包使用）、pages.json.mp（小程序端打包使用），打包时重命名对应的文件为pages.json即可。

打包APP时，首先确认manifest.json - APP原生插件配置 - 本地插件 勾选了 多任务下载管理、DLAN投屏这两个插件，调试时需要先制作自定义调试基座，然后 运行基座选择 - 选择自定义基座 ，然后再运行到手机。

>   前端uniapp目录 common/vmeitime-http/bd.js 文件为加密算法，第35行 
>
>   let kk = jj(ur + k + '75636198730df897ce38c1d176c70f97') 
>
>   中的 `75636198730df897ce38c1d176c70f97` 为密钥，与 api后台 
>   
>   `library/Base.php` 中 `encryptWithOpenssl` 方法内的
>
>   $key = md5(($this->is_https() ? 'https://' : 'http://') . 
>   
>   $_SERVER['SERVER_NAME'] . $_SERVER["REQUEST_URI"] . $key . 
>   
>   '75636198730df897ce38c1d176c70f97');
>
>   $key 后面引号内的字符串需保持一直，否则无法解密。

### 说明

其中的DLAN投屏插件为uniapp插件市场内别人上传的插件包，传送门：https://ext.dcloud.net.cn/plugin?id=1554，感谢作者。



