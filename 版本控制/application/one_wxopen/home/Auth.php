<?php

namespace app\one_wxopen\home;

use app\common\controller\Common;
use app\one_wxopen\service\WeTool;

class Auth extends Common
{
    protected function initialize()
    {
        parent::initialize();
    }
    /**
     * 发起授权
     *
     * @return void
     * @author Leo <13708867890>
     * @since 2020-12-19 18:34:24
     */
    public function index()
    {
        $open =  WeTool::service();
        try {
            $url = $open->getAuthRedirect(get_domain() . '/one_wxopen/auth/callback');
            echo "<h2><a href='{$url}'>授权已有小程序或公众号</a></h2>";
            echo "<h2><a href='javascript::void(0)'>快速注册小程序</a></h2>";
            echo "<h2><a href='javascript::void(0)'>服务商进件模式,快速申请微信支付,可调费率</a></h2>";
            $path = env('app_path') . implode(DIRECTORY_SEPARATOR, ['one_wxopen', 'mpdata']) . DIRECTORY_SEPARATOR;
            $file = glob($path . 'wx????????????????.ini', GLOB_BRACE);
            $_data = [];
            echo '<style>table,table tr th, table tr td { border:1px solid #009900; }</style><table style="border-collapse:collapse;"  border="1" cellspacing="0" cellpadding="0"><tr><th>授权appid</th><th>时间</th><th>详情</th></tr>';
            foreach ($file as $k => $v) {
                if (file_exists($v) === true) {
                    $_content = @parse_ini_file($v, true);
                    array_push($_data, $_content);
                    echo "<tr>
                <td>{$_content['AuthorizerAppid']}</td>
                <td>" . date('Y-m-d H:i:s', $_content['CreateTime']) . "</td>
                <td><a href='" . url('detail', ['appid' => $_content['AuthorizerAppid']]) . "'>查看</a></td>
                </tr>";
                }
            }
            echo '</table>';
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    /**
     * 授权回调
     *
     * @return void
     * @author Leo <13708867890>
     * @since 2020-12-19 18:34:08
     */
    public function callback()
    {
        return $this->redirect(url('index'));
    }
    /**
     * 详细信息
     *
     * @param [type] $appid
     * @return void
     * @author Leo <13708867890>
     * @since 2020-12-19 18:34:33
     */
    public function detail($appid)
    {
        $open =  WeTool::service();
        try {
            $info = $open->getAuthorizerInfo($appid);
            echo "<pre>" . json_encode($info, 448) . "</pre>";
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        // 获取粉丝列表 或者更多操作
        // $wechat = $open->instance('User',$appid,'WeChat');
        // $wechat->getUserList()
    }
}
