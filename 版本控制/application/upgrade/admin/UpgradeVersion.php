<?php

namespace app\upgrade\admin;

use app\system\admin\Admin;

class UpgradeVersion extends Admin
{

    protected $oneModel = 'UpgradeVersion';

    protected function initialize()
    {
        parent::initialize();
        $this->UpgradeVersionModel = model('UpgradeVersion');
    }

    /**
     * 版本控制列表
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function index()
    {
        if (request()->isAjax()) {
            $map = $data = [];
            $page   = input('param.page/d', 1);
            $limit  = input('param.limit/d', 15);
            $status = input('param.status');
            if (is_numeric($status)) {
                $map['status'] = $status;
            }
            $data = $this->UpgradeVersionModel->pageList($map, true, 'create_time desc', $page, $limit, ['status_text']);
            return $this->success('获取成功', '', $data);
        }
        return $this->fetch();
    }
    /**
     * 通知站点更新
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function noticeVersion()
    {
        if (request()->isPost()) {
            $_data = $this->params;
            $result = $this->UpgradeVersionModel->noticeVersion($_data);
            if (false === $result) {
                return $this->error($this->UpgradeVersionModel->getError());
            }
            return $this->success('通知成功');
        }
    }
}
