<?php

namespace app\member\api;

use app\member\model\Login;
use app\member\model\Member;
use app\one_api\api\ApiInit;

class Register extends ApiInit
{
    public function initialize()
    {
        parent::initialize();
    }
    //注册协议
    public function register_agree()
    {
        # code...
    }
    //用户名密码邮箱注册 改传 account password
    public function account_register()
    {
        $data = $this->params;
        $user['salt']        = random(6, 0);
        if (!isset($data['account'])) {
            return $this->_error('帐号信息必填');
        }
        if (!isset($data['password'])) {
            return $this->_error('密码必填');
        }
        if (is_email($data['account'])) { // 邮箱
            $user['email']       = $data['account'];
            $user['username'] = 'U'.random(6,0);
        } elseif (is_mobile($data['account'])) { // 手机号
            $user['mobile']      = $data['account'];
            $user['username'] = 'U'.random(6,0);
        } elseif (is_username($data['account'])) { // 用户名
            $user['username']    = $data['account'];
        }else{
            return $this->_error('帐号应为用户名|手机|邮箱');
        }
        $user['nickname']  = 'N_'.random(6,0);
        $user['salt'] = random(6,0);
        $user['password'] = $data['password'];
        $validate = new \app\member\validate\Member;

        if (!$validate->check($user)) {
            return $this->_error($validate->getError());
        }
        $model = new Login();
        $rs = $model->register($user,$data);
        //自动登录
        action('member/login/login',$data,'api');

    }
    //手机号注册
    public function mobile_register()
    {
        # code...
    }
}
