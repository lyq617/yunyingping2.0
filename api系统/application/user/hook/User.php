<?php
namespace app\user\hook;

use app\user\model\User as UserModel;
use app\user\model\UserToken as UserTokenModel;

class User
{
    /**
     * 注册后操作
     *
     * @param [type] $data
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function userAfterRegister($data) {
        
    }
    /**
     * 更新后
     *
     * @param [type] $user
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function userUpdate($user) {
        
    }
    /**
     * 删除后
     *
     * @param [type] $user
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function userDelete($user) {
        
    }
    /**
     * 登陆后
     *
     * @param [type] $user
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function userAfterLogin($user) {
        // 登录后删除其他token
        // $UserTokenModel = new UserTokenModel();
        // $map[] = ['token', 'neq', $user['token']];
        // $map[] = ['uid', 'eq', $user['id']];
        // $UserTokenModel->where($map)->delete();
    }
}