<?php

namespace app\user\admin;

use app\system\admin\Admin;
use app\user\model\UserBehavior as UserBehaviorModel;

/**
 * 会员分组控制器
 * @package app\user\admin
 */
class Behavior extends Admin
{
    protected $oneModel = 'UserBehavior';

    /**
     * 
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function index()
    {
        if ($this->request->isAjax()) {

            $data = $map = [];
            $keyword    = $this->request->param('keyword/s');
            if ($keyword) {
                $map[] = ['uid|remark', 'like', "%$keyword%"];
            }
            $map[] = ['status', 'eq', 0];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $limit  = isset($data['limit']) ? $data['limit'] : 15;
            $data = (new UserBehaviorModel)->getList($map, $page, $limit, 'update_time desc', true, ['typeText']);
            return $this->success('获取成功', '', $data);
        }
        return $this->fetch();
    }

    public function counts()
    {
        $sql = (new UserBehaviorModel)->field('count(uid) as counts, uid')->group('uid')->order('counts desc')->fetchSql(true)->select();
        $info = (new UserBehaviorModel)->query($sql);
        $str = '';
        foreach ($info as $key => $val) {
            $str .= '用户id：' . $val['uid'] . '，共截图：' . $val['counts'] . "次；<br>";
        }
        return $this->success('获取成功', '', $str);
    }
}
