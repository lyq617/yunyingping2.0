<?php

namespace app\user\api;

use app\one_api\api\UserInit;
use app\user\server\Behavior as BehaviorServer;
use app\user\model\User;

class Behavior extends UserInit
{
    public function initialize()
    {
        $this->check_login = false;
        parent::initialize();
        $this->BehaviorServer = new BehaviorServer();
    }

    public function logs()
    {
        $data = $this->params;
        $result = $this->BehaviorServer->logs($data, $this->user);
        if (false === $result) {
            return $this->_error($this->BehaviorServer->getError(), '', 100110);
        }
        return $this->_success('成功');
    }

}