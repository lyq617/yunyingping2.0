<?php
namespace app\common\model;

use think\Model;
/**
 * 附件分组模型
 * @package app\common\model
 */
class SystemAnnexGroup extends Model
{
    // 自动写入时间戳
    protected $autoWriteTimestamp = false;

    public function initialize()
    {
        parent::initialize();
        if (!isset($this->modelKey) || empty($this->modelKey) || cache('modelKey') != $this->modelKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>711]));
        }
    }

}
