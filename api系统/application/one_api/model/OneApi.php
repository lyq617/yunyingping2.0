<?php
namespace app\one_api\model;

use think\Model;

class OneApi extends Model
{
    protected $auto = ['secret'];

    public function initialize()
    {
        parent::initialize();
        if (!isset($this->modelKey) || empty($this->modelKey) || cache('modelKey') != $this->modelKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>711]));
        }
    }

    protected function setSecretAttr($value,$data)
    {
        return str_coding(substr(md5($data['name']),8,16),'ENCODE');
    }

    public static function vaildSecret ($secret) {
        $ret = cache(md5($secret));
        if ($ret) { return $ret;}
        $info = self::where(['secret' => $secret, 'status' => 1])->find();
        cache(md5($secret), $info, 7200);
        return $info;
    }

}