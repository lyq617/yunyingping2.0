<?php
namespace app\videos\api;
use app\one_api\api\UserInit;
use app\videos\server\Report as ReportServer;

class Report extends UserInit
{

    public function initialize() 
    {
        $this->check_login = false;
        parent::initialize();
        if (!isset($this->apiKey) || empty($this->apiKey) || cache('apiKey') != $this->apiKey) {
            return $this->_error('非法请求', [], 710);
        }
        $this->ReportServer = new ReportServer();
    }

    /**
     * 获取解析地址
     *
     * @param [type] $data
     * @param [type] $user
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function report()
    {
        $data= $this->params;
        $result = $this->ReportServer->report($data, $this->user);
        if (false === $result) {
            return $this->_error($this->ReportServer->getError(),'',80001);
        }
        return $this->_success("成功", $result);
    }

}