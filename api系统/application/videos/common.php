<?php
//多维数组排序
function arrayMsort(&$data, $field)
{
    $regions = array_column($data, $field);
    utf8_array_asort($regions);
    array_multisort($regions, SORT_ASC, $data);
}
function utf8_array_asort(&$array)
{
    if (!isset($array) || !is_array($array)) {
        return false;
    }
    foreach ($array as $k => $v) {
        $array[$k] = iconv('UTF-8', 'GBK//IGNORE', $v);
    }
    return true;
}
/* 取中间字符 */
function getSubstr($str, $leftStr, $rightStr)
{
    $left = strpos($str, $leftStr);
    $right = strpos($str, $rightStr, $left);
    if ($left < 0 or $right < $left) return '';
    return substr($str, $left + strlen($leftStr), $right - $left - strlen($leftStr));
}
/* 取右边字符 */
function getRightStr($str, $rightStr)
{
    $right = strpos($str, $rightStr);
    if (!$right) return '';
    return substr($str, $right + strlen($rightStr));
}
/* 取左边字符 */
function getLeftStr($str, $leftStr)
{
    $left = strpos($str, $leftStr);
    if (!$left) return '';
    return substr($str, 0, $left);
}
/**
 * 检测敏感词
 *
 * @param string $checkStr
 * @param boolean $full 是否全词匹配，默认只要字符串包含敏感词就屏蔽
 * @return void
 * @author 617 <email：723875993@qq.com>
 */
function checkWords($checkStr, $full = false)
{
    $file_path = ROOT_PATH . "public/badwords/badwords.txt";
    if (file_exists($file_path)) {
        $str = file_get_contents($file_path); //将整个文件内容读入到一个字符串中
        $badWord = explode("\r\n", $str);
    }
    $badword1 = array_combine($badWord, array_fill(0, count($badWord), '*'));
    $str = strtr($checkStr, $badword1);
    if (strpos($str, '*') !== false) {
        if ($full) {
            if (strlen($str) == 1) {
                return true;
            }
            return false;
        }
        return true;
    } else {
        return false;
    }
}

function autowrap($fontsize, $angle, $fontface, $string, $width)
{
    // 这几个变量分别是 字体大小, 角度, 字体名称, 字符串, 预设宽度
    $content = "";
    // 将字符串拆分成一个个单字 保存到数组 letter 中
    for ($i = 0; $i < mb_strlen($string); $i++) {
        $letter[] = mb_substr($string, $i, 1);
    }
    foreach ($letter as $l) {
        $teststr = $content . " " . $l;
        $testbox = imagettfbbox($fontsize, $angle, $fontface, $teststr);
        // 判断拼接后的字符串是否超过预设的宽度
        if (($testbox[2] > $width) && ($content !== "")) {
            $content .= "\n";
        }
        $content .= $l;
    }
    return $content;
}


/**
 * 生成宣传海报
 * @param array  参数,包括图片和文字
 * @param string $filename 生成海报文件名,不传此参数则不生成文件,直接输出图片
 * @return [type] [description]
 */
function createPoster($config = array(), $filename = "")
{
    //如果要看报什么错，可以先注释调这个header
    //if(empty($filename)) header("content-type: image/png");
    if (empty($filename)) header("content-type: image/png");
    $imageDefault = array(
        'left' => 0,
        'top' => 0,
        'right' => 0,
        'bottom' => 0,
        'width' => 100,
        'height' => 100,
        'opacity' => 100
    );
    $textDefault = array(
        'text' => '',
        'left' => 0,
        'top' => 0,
        'fontSize' => 32, //字号
        'fontColor' => '255,255,255', //字体颜色
        'angle' => 0,
    );
    $background = $config['background']; //海报最底层得背景
    //背景方法
    $backgroundInfo = getimagesize($background);
    $backgroundFun = 'imagecreatefrom' . image_type_to_extension($backgroundInfo[2], false);
    $background = $backgroundFun($background);
    $backgroundWidth = imagesx($background); //背景宽度
    $backgroundHeight = imagesy($background); //背景高度
    $imageRes = imageCreatetruecolor($backgroundWidth, $backgroundHeight);
    $color = imagecolorallocate($imageRes, 0, 0, 0);
    imagefill($imageRes, 0, 0, $color);
    imagecopyresampled($imageRes, $background, 0, 0, 0, 0, imagesx($background), imagesy($background), imagesx($background), imagesy($background));
    //处理了图片
    if (!empty($config['image'])) {
        foreach ($config['image'] as $key => $val) {
            $val = array_merge($imageDefault, $val);
            $info = getimagesize($val['url']);
            $function = 'imagecreatefrom' . image_type_to_extension($info[2], false);
            if ($val['stream']) { //如果传的是字符串图像流
                $info = getimagesizefromstring($val['url']);
                $function = 'imagecreatefromstring';
            }
            $res = $function($val['url']);
            $resWidth = $info[0];
            $resHeight = $info[1];
            //建立画板 ，缩放图片至指定尺寸
            $canvas = imagecreatetruecolor($val['width'], $val['height']);
            imagefill($canvas, 0, 0, $color);
            //如果是透明的gif或png做透明处理
            $ext = pathinfo($val['url']);
            if (array_key_exists('extension', $ext)) {
                if ($ext['extension'] == 'gif' || $ext['extension'] == 'png') {
                    imageColorTransparent($canvas, $color); //颜色透明

                }
            }
            //关键函数，参数（目标资源，源，目标资源的开始坐标x,y, 源资源的开始坐标x,y,目标资源的宽高w,h,源资源的宽高w,h）
            imagecopyresampled($canvas, $res, 0, 0, 0, 0, $val['width'], $val['height'], $resWidth, $resHeight);
            //$val['left'] = $val['left']<0?$backgroundWidth- abs($val['left']) - $val['width']:$val['left'];
            //如果left小于-1我这做成了计算让其水平居中
            if ($val['left'] < 0) {
                $val['left'] = ceil($backgroundWidth - $val['width']) / 2;
            }
            $val['top'] = $val['top'] < 0 ? $backgroundHeight - abs($val['top']) - $val['height'] : $val['top'];
            //放置图像
            imagecopymerge($imageRes, $canvas, $val['left'], $val['top'], $val['right'], $val['bottom'], $val['width'], $val['height'], $val['opacity']); //左，上，右，下，宽度，高度，透明度

        }
    }
    //处理文字
    if (!empty($config['text'])) {
        foreach ($config['text'] as $key => $val) {
            $val = array_merge($textDefault, $val);
            list($R, $G, $B) = explode(',', $val['fontColor']);
            $fontColor = imagecolorallocate($imageRes, $R, $G, $B);
            //$val['left'] = $val['left']<0?$backgroundWidth- abs($val['left']):$val['left'];
            //如果left小于-1我这做成了计算让其水平居中
            $text = autowrap($val['fontSize'], 0, $val['fontPath'], $val['text'], isset($val['width'])? $val['width'] : 390);
            if ($val['left'] < 0) {
                $fontBox = imagettfbbox($val['fontSize'], 0, $val['fontPath'], $text); //文字水平居中实质
                $val['left'] = ceil(($backgroundWidth - $fontBox[2]) / 2); //计算文字的水平位置

            }
            $val['top'] = $val['top'] < 0 ? $backgroundHeight - abs($val['top']) : $val['top'];
            imagettftext($imageRes, $val['fontSize'], $val['angle'], $val['left'], $val['top'], $fontColor, $val['fontPath'], $text);
        }
    }
    //生成图片
    if (!empty($filename)) {
        $res = imagejpeg($imageRes, $filename, 95); //保存到本地
        imagedestroy($imageRes);
        if (!$res) return false;
        return $filename;
    } else {
        header("Content-type:image/png");
        imagejpeg($imageRes); //在浏览器上显示
        imagedestroy($imageRes);
    }
}
