<?php
namespace app\videos\validate;

use think\Validate;
/**
 * 会员分组验证器
 * @package app\user\validate
 */
class VideosDanmu extends Validate
{
    //定义验证规则
    protected $rule = [
        'text|弹幕内容' => 'require|checkText:thinkphp',
        'color|弹幕颜色'  => 'require',
        'time|弹幕时间'  => 'require',
        'vid|视频id'  => 'require',
    ];

    //定义验证提示
    protected $message = [
        'text.require' => '弹幕内容不能为空',
        'color.unique' => '弹幕颜色不能为空',
        'time.require'    => '弹幕时间不能为空',
        'vid.require'    => '视频id不能为空',
    ];


    protected function checkText($value, $rule, $data) {
        if (checkWords($data['text'])) {
            return '含有敏感词';
        }
        return true;
    }
}
