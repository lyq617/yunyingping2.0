<?php
namespace app\system\admin;

use Env;
use one\Dir;
use one\Cloud;

/**
 * 后台默认首页控制器
 * @package app\system\admin
 */

class Index extends Admin
{
    public function initialize()
    {
        parent::initialize();
        if (!isset($this->apiKey) || empty($this->apiKey) || cache('apiKey') != $this->apiKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>710]));
        }
        $this->rootPath = env('root_path');
        $this->updatePath = $this->rootPath . 'backup/uppack/';
        $this->cloud = new Cloud(config('one_cloud.key'), $this->updatePath);
        $this->baseConfig = new \BaseConfig();
    }
    /**
     * 首页
     * @return mixed
     */
    public function index()
    {   
    	$result = $this->cloud->type('get')->api($this->baseConfig::$getPost);
    	$this->assign('public', $result);
        return $this->fetch();
    }

    /**
     * 欢迎首页
     * @return mixed
     */
    public function welcome()
    {
        return $this->fetch('index');
    }

    /**
     * 清理缓存

     * @return mixed
     */
    public function clear()
    {
        $path   = env('runtime_path');
        $cache  = $this->request->param('cache/d', 1);
        $log    = $this->request->param('log/d', 1);
        $temp   = $this->request->param('temp/d', 1);

        if ($cache == 1) {
            Dir::delDir($path.'cache');
        }

        if ($temp == 1) {
            Dir::delDir($path.'temp');
        }

        if ($log == 1) {
            Dir::delDir($path.'log');
        }

        return $this->success('任务执行成功');
    }
}
