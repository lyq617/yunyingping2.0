<?php

namespace app\banner\admin;

use app\system\admin\Admin;
use app\banner\model\Ad as AdModel;
use app\user\model\UserGroup as GroupModel;

class Ad extends Admin
{
    protected $oneModel = 'Ad'; //模型名称[通用添加、修改专用]
    protected $oneTable = ''; //表名称[通用添加、修改专用]
    protected $oneAddScene = ''; //添加数据验证场景名
    protected $oneEditScene = ''; //更新数据验证场景名

    public function initialize()
    {
        parent::initialize();
        $this->AdModel = new AdModel();
    }

    /**
     * 广告列表
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $map    = $data = [];
            $data   = input();
            $page   = isset($data['page']) ? $data['page'] : 1;
            $limit  = isset($data['limit']) ? $data['limit'] : 15;

            $data = $this->AdModel->getList($map, $page, $limit);
            return $this->success('获取成功', '', $data);
        }
        $this->assign('type', 1);
        return $this->fetch();
    }
    /**
     * 添加广告
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function edit($id = 0)
    {
        $data = [];
        $data = input();
        $_type = [
            [
                'label' => '启动页',
                'value' => 1
            ],
            [
                'label' => '全局普通广告（banner、原生格子广告等）',
                'value' => 2
            ],
            [
                'label' => '播放页弹窗',
                'value' => 3
            ],
            [
                'label' => '播放页视频组件上方',
                'value' => 8
            ],
            [
                'label' => '播放页视频组件下方',
                'value' => 4
            ],
            [
                'label' => '播放前贴片广告',
                'value' => 5
            ],
            [
                'label' => '播放页激励广告',
                'value' => 6
            ],
            [
                'label' => '壁纸激励广告',
                'value' => 7
            ],
        ];
        $_options = [
            [
                'label' => '自定义广告',
                'options' => [
                    [
                        'label' => '自定义图片',
                        'value' => 1
                    ],
                    [
                        'label' => '自定义视频',
                        'value' => 2
                    ],
                ],
            ],
            [
                'label' => 'WeChat小程序广告',
                'options' => [
                    [
                        'label' => 'WeChat-小程序原生模板广告',
                        'value' => 9
                    ],
                    [
                        'label' => 'WeChat-小程序BANNER广告',
                        'value' => 3
                    ],
                    [
                        'label' => 'WeChat-小程序视频广告',
                        'value' => 4
                    ],
                    [
                        'label' => 'WeChat-小程序插屏广告',
                        'value' => 5
                    ],
                    [
                        'label' => 'WeChat-小程序格子广告',
                        'value' => 6
                    ],
                    [
                        'label' => 'WeChat-小程序视频贴片广告',
                        'value' => 7
                    ],
                    [
                        'label' => 'WeChat-小程序激励式广告',
                        'value' => 8
                    ],
                ],
            ],

        ];
        $type = isset($data['type']) && !empty($data['type']) ? $data['type'] : 1;
        if ($this->request->isAjax()) {
            $img = is_array($data['img']) ? implode(',', $data['img']) :  $data['img'];
            $_data = [
                'title' => $data['title'],
                'img' => $img,
                'url' => $data['url'],
                'v_url' => $data['v_url'],
                'desc' => isset($data['desc']) ? $data['desc'] : '',
                'type' => $data['type'],
                'ad_type' => $data['ad_type'],
            ];
            if (false !== $this->AdModel->isUpdate($data['id'] > 0 ? true : false)->save($_data)) {
                return $this->success('保存成功');
            }
            return $this->error('保存失败' . $this->AdModel->getError());
        }
        $formData = $this->AdModel->where('id', $id)->find();
        $this->assign('formData', $formData ? $formData->toArray() : []);
        $this->assign('type', $type);
        $this->assign('options', $_options);
        $this->assign('type', $_type);
        $this->assign('groupData', GroupModel::getGroupList(true));
        return $this->fetch('form');
    }
}
